
<?php
require('fpdf.php');
$datedebut = $_POST['datedebut'];

try {
	$db = new SQLite3('../../../db/db2sp.db');
}
catch (Exception $exception) {
	echo '<p>Error connecting to the database!</p>';
}

class PDF extends FPDF
{
	// En tête
	function Header()
	{
		$this->Image('img/logo.png',10,6,50);
		$this->SetFont('Arial','B',15);
		$this->SetX(0);
		$this->Cell(80);
		$this->Cell(55,10,utf8_decode('Fichier de traçabilité'),1,0,'C');
		$this->Text(10,50,"Date début : ".$datedebut);
		$this->Ln(20);
	}

	function Footer()
	{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}

	function FancyTable($header, $data)
	{
		$this->SetX(25);
	    $this->SetFillColor(116, 208, 241);
	    $this->SetTextColor(255);
	    $this->SetDrawColor(30, 127, 203);
	    $this->SetLineWidth(.3);
	    $this->SetFont('','B');
	    // En-tête
	    $w = array(40, 35, 45, 40);
	    for($i=0;$i<count($header);$i++)
	        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
	    $this->Ln();
	    $this->SetFillColor(224,235,255);
	    $this->SetTextColor(0);
	    $this->SetFont('');
	    // Données
	    $fill = false;
	    // foreach($data as $row)
	    // {
	    //     $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
	    //     $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
	    //     $this->Cell($w[2],6,number_format($row[2],0,',',' '),'LR',0,'R',$fill);
	    //     $this->Cell($w[3],6,number_format($row[3],0,',',' '),'LR',0,'R',$fill);
	    //     $this->Ln();
	    //     $fill = !$fill;
	    // }
	    $this->SetX(25);
	    $this->Cell(array_sum($w),0,'','T');
	}
}

$header = array('Date', 'Cuve', 'Zone', 'Niveau');
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->FancyTable($header);
$pdf->SetFont('Times','',12);
$pdf->Output();

?>