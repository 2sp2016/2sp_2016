<?php
namespace dashboard\Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class PdfController {

	public function PdfAction(Request $request, Application $app) {
		return $app['twig']->render('pdf.html.twig' , array(
			'error'         => $app['security.last_error']($request),
			'last_username' => $app['session']->get('_security.last_username'),
			));
	}

	public function PdfGeneration(Application $app){

	}
}