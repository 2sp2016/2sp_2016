<?php

namespace dashboard\controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class GraphController {
	
	public function showSampleAction(Request $request, Application $app) {
		return $app['twig']->render('graph.html.twig' , array(
			'error'         => $app['security.last_error']($request),
			'last_username' => $app['session']->get('_security.last_username'),
			));
	}

	public function retrieveData(Application $app) {
		$date = time() * 1000;
		$data = rand(100, 200);
		return $app->json(array($date, $data));
	}

	public function showArduinoAction(Request $request, Application $app) {
		return $app['twig']->render('grapharduino.html.twig', array(
			'error'		    => $app['security.last_error']($request),
			'last_username' => $app['session']->get('_security.last_username'),
			));
	}

	public function retrieveDataArduino(Application $app) {

		try {
			$db = new \SQLite3('../db/db2sp.db');
			$dataSet1 = $db->query('SELECT * FROM arduino ORDER BY id DESC LIMIT 1;');
			$db = null;
		}
		catch (Exception $exception) {
			echo '<p>Error connecting to the database!</p>';
		}

		if($dataSet1) {
	// récupére les valeurs du dernier enregistrement
			$endRow = $dataSet1->fetchArray();
	//Si la bdd est vide le tableau sera [-1,-1], aucun point ne sera affiché
			if (! $endRow) {
				$ret = array(-1,-1);
			}
			else {
		// Créer un tableau PHP et l'envoyer en JSON
				$delta = 0 + $endRow['temp'];
				$t = 0 + $endRow['timestamp'];
				$ret = array($t, $delta);
			} 	
		}

		return $app->json($ret);
		
	}

}	