<?php

namespace dashboard\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CuveMonitoreeType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('cuve', 'choice')
			->add('moduleBLE', 'choice')
			->add('seuilMin', 'number')
			->add('seuilMax', 'number');	
	}

	public function getName()
	{
		return 'cuvemonitoree';
	}
}