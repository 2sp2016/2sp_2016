<?php

namespace dashboard\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;


class CuveType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('reference', 'text')
			->add ('capacite', 'number')
			->add ('dateachat', 'date', array(
				'widget' => 'single_text',
				'input' => 'string',
				'format' => 'yyyy-MM-dd'
				))
			->add ('datecontrole', 'date', array(
				'widget' => 'single_text',
				'input' => 'string',
				'format' => 'yyyy-MM-dd'
				));
	}

	public function getName()
	{
		return 'cuve';
	}
}