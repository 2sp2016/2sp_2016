<?php

namespace dashboard\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ModuleBLEType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('adresseMAC', 'text')
			->add ('reference', 'text');
	}

	public function getName()
	{
		return 'moduleBLE';
	}
}