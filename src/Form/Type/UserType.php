<?php
namespace dashboard\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array(
                'label' => 'Identifiant'
                ))
            ->add('password', 'repeated', array(
                'type'            => 'password',
                'invalid_message' => 'Les champs de mot de passe doivent correspondre.',
                'options'         => array('required' => true),
                'first_options'   => array('label' => 'Mot de Passe'),
                'second_options'  => array('label' => 'Répéter le mot de passe'),
            ))
            ->add('role', 'choice', array(
                'choices' => array('1' => 'Administrateur', '2' => 'Utilisateur')
            ));
    }
    public function getName()
    {
        return 'user';
    }
}