<?php

namespace dashboard\Domain;

class Cuve
{
	private $id;
	private $reference;
	private $capacite;
	private $dateAchat;
	private $dateControle;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getReference() {
		return $this->reference;
	}

	public function setReference($reference){
		$this->reference = $reference;
	}

	public function getCapacite() {
		return $this->capacite;
	}

	public function setCapacite($capacite) {
		$this->capacite = $capacite;
	}

	public function getDateAchat() {
		return $this->dateAchat;
	}

	public function setDateAchat($dateAchat) {
		$this->dateAchat = $dateAchat;
	}

	public function getDateControle() {
		return $this->dateControle;
	}

	public function setDateControle($dateControle) {
		$this->dateControle = $dateControle;
	}
}