<?php

namespace dashboard\Domain;

class Cuvemonitoree 
{
	private $id;
	private $seuilMin;
	private $seuilMax;
	private $cuve;
	private $moduleBLE;

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getSeuilMin() {
		return $this->seuilMin;
	}

	public function setSeuilMin($seuilMin) {
		$this->seuilMin = $seuilMin;
	}

	public function getSeuilMax() {
		return $this->seuilMax;
	}

	public function setSeuilMax($seuilMax) {
		$this->seuilMax = $seuilMax;
	}

	public function getCuve() {
		return $this->cuve;
	}

	public function setCuve(Cuve $cuve) {
		$this->cuve = $cuve;
	}

	public function getModuleBLE() {
		return $this->moduleBLE;
	}

	public function setModuleBLE(ModuleBLE $moduleBLE) {
		$this->moduleBLE = $moduleBLE;
	}

}