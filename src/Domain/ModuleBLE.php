<?php

namespace dashboard\Domain;

class ModuleBLE {

	private $id;
	private $adresseMAC;
	private $reference;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getAdresseMAC() {
		return $this->adresseMAC;
	}

	public function setAdresseMAC($adresseMAC) {
		$this->adresseMAC = $adresseMAC;
	}

	public function getReference() {
		return $this->reference;
	}

	public function setReference($reference) {
		$this->reference = $reference;
	}
}