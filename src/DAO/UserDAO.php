<?php

namespace dashboard\DAO;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

use dashboard\Domain\User;

class UserDAO extends DAO implements UserProviderInterface
{
    /**
     * Returns a user matching the supplied id.
     *
     * @param integer $id The user id.
     *
     * @return \dashboard\Domain\User|throws an exception if no matching user is found
     */
    public function find($id) {
        $sql = "select * from Utilisateurs inner join Roles on Roles_idRole=idRole where idUtilisateur=?";
        $row = $this->getDb()->fetchAssoc($sql, array($id));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new \Exception("No user matching id " . $id);
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $sql = "select * from Utilisateurs inner join Roles on Roles_idRole=idRole where nom=?";
        $row = $this->getDb()->fetchAssoc($sql, array($username));

        if ($row)
            return $this->buildDomainObject($row);
        else
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
    }

    /**
     * Saves a user into the database.
     *
     * @param \dashboard\Domain\User $user The user to save
	  */
    public function save(User $user) {
        $userData = array(
            'nom' => $user->getUsername(),
            'salage' => $user->getSalt(),
            'motDePasse' => $user->getPassword(),
            'Roles_idRole' => $user->getRole()
            );

        if ($user->getId()) {
            // The user has already been saved : update it
            $this->getDb()->update('Utilisateurs', $userData, array('idUtilisateur' => $user->getId()));
        } else {
            // The user has never been saved : insert it
            $this->getDb()->insert('Utilisateurs', $userData);
            // Get the id of the newly created user and set it on the entity.
            $id = $this->getDb()->lastInsertId();
            $user->setId($id);
        }
	 }

    /**
     * Returns a list of all users, sorted by role and name.
     *
     * @return array A list of all users.
     */
    public function findAll() {
        $sql = "select * from Utilisateurs inner join Roles on Roles_idRole=idRole";
        $result = $this->getDb()->fetchAll($sql);

        // Convert query result to an array of domain objects
        $entities = array();
        foreach ($result as $row) {
            $id = $row['idUtilisateur'];
            $entities[$id] = $this->buildDomainObject($row);
        }
        return $entities;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return 'dashboard\Domain\User' === $class;
    }

    /**
     * Removes a user from the database.
     *
     * @param @param integer $id The user id.
     */
    public function delete($id) {
        // Delete the user
        $this->getDb()->delete('Utilisateurs', array('idUtilisateur' => $id));
    }

    /**
     * Creates a User object based on a DB row.
     *
     * @param array $row The DB row containing User data.
     * @return \dashboard\Domain\User
     */
    protected function buildDomainObject($row) {
        $user = new User();
        $user->setId($row['idUtilisateur']);
        $user->setUserName($row['nom']);
        $user->setPassword($row['motDePasse']);
		$user->setRole($row['denomination']);
		$user->setSalt($row['salage']);
        return $user;
    }
}
