<?php

namespace dashboard\DAO;
use dashboard\Domain\ModuleBLE;

class ModuleBLEDAO extends DAO
{
	public function findAll() {
		$sql = "select * from ModulesBLE order by idModuleBLE desc";
		$result = $this->getDb()->fetchall($sql);

		$modulesBLE = array();
		foreach ($result as $row) {
			$moduleBLEId = $row['idModuleBLE'];
			$modulesBLE[$moduleBLEId] = $this->buildDomainObject($row);
		}
		return $modulesBLE;
	}

	public function find($id) {
		$sql = "select * from ModulesBLE where idModuleBLE=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("Aucun ModuleBLE pour cet id" . $id);
			
	}

	public function save(ModuleBLE $moduleBLE) {
		$moduleBLEData = array(
			'adresseMAC' => $moduleBLE->getAdresseMAC(),
			'referenceModule' => $moduleBLE->getReference(),
			);
		if($moduleBLE->getId()) {
			$this->getDb()->update('ModulesBLE', $moduleBLEData, array('idModuleBLE' => $moduleBLE->getId()));
		} else {
			$this->getDb()->insert('ModulesBLE', $moduleBLEData);
			$id = $this->getDb()->lastInsertId();
			$moduleBLE->setId($id);
		}
	}

	public function delete($id) {
		$this->getDb()->delete('ModulesBLE', array('idModuleBLE' => $id));
	}

	protected function buildDomainObject($row) {
		$moduleBLE = new ModuleBLE();
		$moduleBLE->setId($row['idModuleBLE']);
		$moduleBLE->setAdresseMAC($row['adresseMAC']);
		$moduleBLE->setReference($row['referenceModule']);
		return $moduleBLE;
	}
}