<?php

namespace dashboard\DAO;
use dashboard\Domain\Cuve;

class CuveDAO extends DAO
{
	public function findAll() {
		$sql = "select * from Cuves order by idCuve desc";
		$result = $this->getDb()->fetchall($sql);

		$cuves = array();
		foreach ($result as $row) {
			$cuveId = $row['idCuve'];
			$cuves[$cuveId] = $this->buildDomainObject($row);
		}
		return $cuves;
	}

	public function find($id) {
		$sql = "select * from Cuves where idCuve=?";
		$row = $this->getDb()->fetchAssoc($sql, array($id));

		if($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("Aucune Cuves pour cet id" . $id);
			
	}

	public function save(Cuve $cuve) {
		$cuveData = array(
			'referenceConstructeur' => $cuve->getReference(),
			'capacite' => $cuve->getCapacite(),
			'dateAchat' => $cuve->getDateAchat(),
			'dateControle' => $cuve->getDateControle(),
			);
		if($cuve->getId()) {
			$this->getDb()->update('Cuves', $cuveData, array('idCuve' => $cuve->getId()));
		} else {
			$this->getDb()->insert('Cuves', $cuveData);
			$id = $this->getDb()->lastInsertId();
			$cuve->setId($id);
		}
	}

	public function delete($id) {
		$this->getDb()->delete('Cuves', array('idCuve' => $id));
	}

	protected function buildDomainObject($row) {
		$cuve = new Cuve();
		$cuve->setId($row['idCuve']);
		$cuve->setReference($row['referenceConstructeur']);
		$cuve->setCapacite($row['capacite']);
		$cuve->setDateAchat($row['dateAchat']);
		$cuve->setDateControle($row['dateControle']);
		return $cuve;
	}
}