<?php

namespace dashboard\DAO;
use dashboard\Domain\Cuvemonitoree;

class CuvemonitoreeDAO extends DAO
{
	private $cuveDAO;
	private $moduleBLEDAO;

	public function setCuveDAO(CuveDAO $cuveDAO) {
		$this->cuveDAO = $cuveDAO;
	}

	public function setModuleBLEDAO(ModuleBLEDAO $moduleBLEDAO) {
		$this->moduleBLEDAO = $moduleBLEDAO;
	}

	public function findAll() {
		$sql = "select * from CuvesMonitorees order by idCuveMonitoree desc";
		$result = $this->getDb()->fetchAll($sql);

		$entities = array();
		foreach ($result as $row) {
			$id = $row['idCuveMonitoree'];
			$entities[$id] = $this->buildDomainObject($row);
		}
		return $entities;
	}

	public function findAllByCuve($cuveId) {
		$cuve = $this->cuveDAO->find($cuveId);

		$sql = "select idCuveMonitoree, seuilMin, seuilMax, idModuleBLE from CuvesMonitorees where idCuve=? order by idCuveMonitoree";
		$result = $this->getDb()->fetchAll($sql, array($cuveId));

		$cuvesmonitorees = array();
		foreach ($result as $row) {
			$cuvemonitoreeId = $row['idCuveMonitoree'];
			$cuvemonitoree = $this->buildDomainObject($row);

			$cuvemonitoree->setCuve($cuve);
			$cuvesmonitorees[$cuvemonitoreeId] = $cuvemonitoree;
		}
		return $cuvesmonitorees;
	}

	public function find($id) {
		$sql ="select * from CuvesMonitorees where idCuveMonitoree=?";
		$row =$this->getDb()->fetchAssoc($sql, array($id));

		if($row)
			return $this->buildDomainObject($row);
		else
			throw new \Exception("Pas de cuve monitorée pour cet id" . $id);			
	}

	public function save(Cuvemonitoree $cuvemonitoree) {
		$cuvemonitoreeData = array(
			'idCuve' => $cuvemonitoree->getCuve()->getId(),
			'idModuleBLE' => $cuvemonitoree->getModuleBLE()->getId(),
			'seuilMin' => $cuvemonitoree->getSeuilMin(),
			'seuilMax' => $cuvemonitoree->getSeuilMax()
			);
		if($cuvemonitoree->getId()) {
			$this->getDb()->update('CuvesMonitorees', $cuvemonitoreeData, array('idCuveMonitoree' => $cuvemonitoree->getId()));
		}
		else {
			$this->getDb()->insert('CuvesMonitorees', $cuvemonitoreeData);
			$id = $this->getDb()->lastInsertId();
			$cuvemonitoree->setId($id);
		}
	}

	public function delete($id) {

		$this->getDb()->delete('CuvesMonitorees', array('idCuveMonitoree' => $id));
	}

	protected function buildDomainObject($row) {
		$cuvemonitoree = new Cuvemonitoree();
		$cuvemonitoree->setId($row['idCuveMonitoree']);
		$cuvemonitoree->setSeuilMin($row['seuilMin']);
		$cuvemonitoree->setSeuilMax($row['seuilMax']);

		if(array_key_exists('idCuve', $row)) {
			$cuveId = $row['idCuve'];
			$cuve = $this->cuveDAO->find($cuveId);
			$cuvemonitoree->setCuve($cuve);
		}
		if (array_key_exists('idModuleBLE', $row)) {
			$moduleBLEId = $row['idModuleBLE'];
			$moduleBLE = $this->moduleBLEDAO->find($moduleBLEId);
			$cuvemonitoree->setModuleBLE($moduleBLE);
		}
		return $cuvemonitoree;
	}

}