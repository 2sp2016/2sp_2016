<?php

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;

// Register global error and exception handlers
ErrorHandler::register();
ExceptionHandler::register();

// Register service providers
$app->register(new Silex\Provider\DoctrineServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));
$app['twig'] = $app->share($app->extend('twig', function(Twig_Environment $twig, $app) {
    $twig->addExtension(new Twig_Extensions_Extension_Text());
    return $twig;
}));
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'secured' => array(
            'pattern' => '^/',
            'anonymous' => true,
            'logout' => true,
				'form' => array('login_path' => '/login', 'check_path' => '/login_check', 'failure_path' => '/#auth'),
			//	'users' => array(
         //   '2spadmin' => array('ROLE_ADMIN', 'vyNKAnuS5izwquvL83Zn1RNQh9TpO/bRtWoa1cxxdFUXWDVG/8oju6JLOmsWMcrRk2L9h/KN7fThZb7uuXplJA=='),
			//),
            'users' => $app->share(function () use ($app) {
                return new dashboard\DAO\UserDAO($app['db']);
            }),
        ),
    ),
    'security.role_hierarchy' => array(
        'ROLE_ADMIN' => array('ROLE_USER'),
    ),
    'security.access_rules' => array(
        array('^/admin', 'ROLE_ADMIN'),
    ),
));
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/log/dashboard.log',
    'monolog.name' => 'dashboard',
    'monolog.level' => $app['monolog.level']
));

$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app['dao.user'] = $app->share(function ($app) {
    return new dashboard\DAO\UserDAO($app['db']);
});
$app['dao.cuve'] = $app->share(function($app){
    return new dashboard\DAO\CuveDAO($app['db']);
});
$app['dao.moduleBLE'] = $app->share(function($app){
    return new dashboard\DAO\ModuleBLEDAO($app['db']);
});

$app['dao.cuvemonitoree'] = $app->share(function($app){
    $cuvemonitoreeDAO = new dashboard\DAO\CuvemonitoreeDAO($app['db']);
    $cuvemonitoreeDAO->setCuveDAO($app['dao.cuve']);
    $cuvemonitoreeDAO->setModuleBLEDAO($app['dao.moduleBLE']);
    return $cuvemonitoreeDAO;
});