<?php

// Doctrine (db)
$app['db.options'] = array(
    'driver'   => 'pdo_sqlite',
    'charset'  => 'utf8',
    'path'     => __DIR__.'/../../db/db2sp.db'
 );


// enable the debug mode
$app['debug'] = true;

// define log parameters
$app['monolog.level'] = 'INFO';
