<?php
use Symfony\Component\HttpFoundation\Request;
use dashboard\Domain\User;
use dashboard\Form\Type\UserType;
use dashboard\Domain\Cuve;
use dashboard\Form\Type\CuveType;
use dashboard\Domain\ModuleBLE;
use dashboard\Form\Type\ModuleBLEType;
use dashboard\Domain\Cuvemonitoree;
use dashboard\Form\Type\CuveMonitoreeType;

// url page d'accueil
$app->get('/', "dashboard\Controller\HomeController::indexAction")->bind('home');

// url page d'authentification
$app->get('/login', "dashboard\Controller\HomeController::loginAction")->bind('login');

//Courbetest
$app->match('/graphsample', "dashboard\Controller\GraphController::showSampleAction")->bind('graph_sample');

$app->match('/grapharduino', "dashboard\Controller\GraphController::showArduinoAction")->bind('graph_arduino');

$app->match('/graphsample/livedata', "dashboard\Controller\GraphController::retrieveData")->bind('graph_sample_livedata');

$app->match('/grapharduino/livedata', "dashboard\Controller\GraphController::retrieveDataArduino")->bind('graph_arduino_livedata');

$app->match('/pdf', "dashboard\Controller\PdfController::PdfAction")->bind('pdf');

$app->match('/pdfgeneration', "dashboard\Controller\PdfController::PdfGeneration")->bind('pdf_generation');

// Admin home page
$app->get('/admin', function() use ($app) {
    $users = $app['dao.user']->findAll();
    $cuves = $app['dao.cuve']->findAll();
    $cuvesmonitorees = $app['dao.cuvemonitoree']->findAll();
    $modulesBLE = $app['dao.moduleBLE']->findAll();
        return $app['twig']->render('admin.html.twig', array(
        'cuves' => $cuves,
        'modulesBLE' => $modulesBLE,
        'cuvesmonitorees' => $cuvesmonitorees,
        'users' => $users));
})->bind('admin');

// Add a user
$app->match('/admin/user/add', function(Request $request) use ($app) {
    $user = new User();
    $userForm = $app['form.factory']->create(new UserType(), $user);
    $userForm->handleRequest($request);
    if ($userForm->isSubmitted() && $userForm->isValid()) {
        // generate a random salt value
        $salt = substr(md5(time()), 0, 23);
        $user->setSalt($salt);
        $plainPassword = $user->getPassword();
        // find the default encoder
        $encoder = $app['security.encoder.digest'];
        // compute the encoded password
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password); 
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', "L'utilisateur a été créé avec succès.");
    }
    return $app['twig']->render('user_form.html.twig', array(
        'title' => 'Nouvel Utilisateur',
        'userForm' => $userForm->createView()));
})->bind('admin_user_add');

// Edit an existing user
$app->match('/admin/user/{id}/edit', function($id, Request $request) use ($app) {
    $user = $app['dao.user']->find($id);
    $userForm = $app['form.factory']->create(new UserType(), $user);
    $userForm->handleRequest($request);
    if ($userForm->isSubmitted() && $userForm->isValid()) {
        $plainPassword = $user->getPassword();
        // find the encoder for the user
        $encoder = $app['security.encoder_factory']->getEncoder($user);
        // compute the encoded password
        $password = $encoder->encodePassword($plainPassword, $user->getSalt());
        $user->setPassword($password); 
        $app['dao.user']->save($user);
        $app['session']->getFlashBag()->add('success', "L'utilisateur a été modifié avec succès.");
    }
    return $app['twig']->render('user_form.html.twig', array(
        'title' => "Modification d'un Utilisateur",
        'userForm' => $userForm->createView()));
})->bind('admin_user_edit');

// Remove a user
$app->get('/admin/user/{id}/delete', function($id, Request $request) use ($app) {
    // Delete the user
    $app['dao.user']->delete($id);
    $app['session']->getFlashBag()->add('success', 'L`utilisateur a été supprimé avec succès.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_user_delete');

// Add a new cuve
$app->match('/admin/cuve/add', function(Request $request) use ($app) {
    $cuve = new Cuve();
    $cuveForm = $app['form.factory']->create(new CuveType(), $cuve);
    $cuveForm->handleRequest($request);
    if ($cuveForm->isSubmitted() && $cuveForm->isValid()) {
        $app['dao.cuve']->save($cuve);
        $app['session']->getFlashBag()->add('success', ' La cuve a été créée avec succès.');
    }
    return $app['twig']->render('cuve_form.html.twig', array(
        'title' => 'Nouvelle Cuve',
        'cuveForm' => $cuveForm->createView()));
})->bind('admin_cuve_add');

// Edit an existing cuve
$app->match('/admin/cuve/{id}/edit', function($id, Request $request) use ($app) {
    $cuve = $app['dao.cuve']->find($id);
    $cuveForm = $app['form.factory']->create(new CuveType(), $cuve);
    $cuveForm->handleRequest($request);
    if ($cuveForm->isSubmitted() && $cuveForm->isValid()) {
        $app['dao.cuve']->save($cuve);
        $app['session']->getFlashBag()->add('success', 'La cuve a été modifiée avec succès.');
    }
    return $app['twig']->render('cuve_form.html.twig', array(
        'title' => "Modification d'une Cuve",
        'cuveForm' => $cuveForm->createView()));
})->bind('admin_cuve_edit');

// Remove an cuve
$app->get('/admin/cuve/{id}/delete', function($id, Request $request) use ($app) {
    $app['dao.cuve']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The cuve was succesfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_cuve_delete');

// Add a new module
$app->match('/admin/moduleBLE/add', function(Request $request) use ($app) {
    $moduleBLE = new ModuleBLE();
    $moduleBLEForm = $app['form.factory']->create(new ModuleBLEType(), $moduleBLE);
    $moduleBLEForm->handleRequest($request);
    if ($moduleBLEForm->isSubmitted() && $moduleBLEForm->isValid()) {
        $app['dao.moduleBLE']->save($moduleBLE);
        $app['session']->getFlashBag()->add('success', 'The article was successfully created.');
    }
    return $app['twig']->render('moduleBLE_form.html.twig', array(
        'title' => 'Nouveau Module',
        'moduleBLEForm' => $moduleBLEForm->createView()));
})->bind('admin_moduleBLE_add');

// Edit an existing module
$app->match('/admin/moduleBLE/{id}/edit', function($id, Request $request) use ($app) {
    $moduleBLE = $app['dao.moduleBLE']->find($id);
    $moduleBLEForm = $app['form.factory']->create(new ModuleBLEType(), $moduleBLE);
    $moduleBLEForm->handleRequest($request);
    if ($moduleBLEForm->isSubmitted() && $moduleBLEForm->isValid()) {
        $app['dao.moduleBLE']->save($moduleBLE);
        $app['session']->getFlashBag()->add('success', 'The cuve was succesfully updated.');
    }
    return $app['twig']->render('moduleBLE_form.html.twig', array(
        'title' => 'Edit module',
        'moduleBLEForm' => $moduleBLEForm->createView()));
})->bind('admin_moduleBLE_edit');

// Remove an module
$app->get('/admin/moduleBLE/{id}/delete', function($id, Request $request) use ($app) {
    $app['dao.moduleBLE']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The cuve was succesfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_moduleBLE_delete');

// Add a new cuve monitoree
$app->match('/admin/cuvemonitoree/add', function(Request $request) use ($app) {
    $cuvemonitoree = new Cuvemonitoree();
    $cuvemonitoreeForm = $app['form.factory']->create(new CuveMonitoreeType(), $cuvemonitoree);
    $cuvemonitoreeForm->handleRequest($request);
    if ($cuvemonitoreeForm->isSubmitted() && $cuvemonitoreeForm->isValid()) {
        $app['dao.cuvemonitoree']->save($cuvemonitoree);
        $app['session']->getFlashBag()->add('success', 'The article was successfully created.');
    }
    return $app['twig']->render('cuvemonitoree_form.html.twig', array(
        'title' => 'Nouvelle Cuve Monitorée',
        'cuvemonitoreeForm' => $cuvemonitoreeForm->createView()));
})->bind('admin_cuvemonitoree_add');

// Edit an existing module
$app->match('/admin/cuvemonitoree/{id}/edit', function($id, Request $request) use ($app) {
    $cuvemonitoree = $app['dao.cuvemonitoree']->find($id);
    $cuvemonitoreeForm = $app['form.factory']->create(new CuveMonitoreeType(), $cuvemonitoree);
    $cuvemonitoreeForm->handleRequest($request);
    if ($cuvemonitoreeForm->isSubmitted() && $cuvemonitoreeForm->isValid()) {
        $app['dao.cuvemonitoree']->save($cuvemonitoree);
        $app['session']->getFlashBag()->add('success', 'The cuve was succesfully updated.');
    }
    return $app['twig']->render('cuvemonitoree_form.html.twig', array(
        'title' => 'Edit Cuve Monitorée',
        'cuvemonitoreeForm' => $cuvemonitoreeForm->createView()));
})->bind('admin_cuvemonitoree_edit');

// Remove an module
$app->get('/admin/cuvemonitoree/{id}/delete', function($id, Request $request) use ($app) {
    $app['dao.cuvemonitoree']->delete($id);
    $app['session']->getFlashBag()->add('success', 'The cuve was succesfully removed.');
    // Redirect to admin home page
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('admin_cuvemonitoree_delete');